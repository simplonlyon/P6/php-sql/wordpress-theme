<?php 
/*
Template Name: Liste des Boissons
*/

?>
<?php

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
            <?php
            while ( have_posts() ) : the_post();
                echo '<h1>';
                the_title();
                echo '</h1>';

                echo '<p>';
                the_content();
                echo '</p>';
            endwhile;

            echo '<section class="drinks">';
            //On fait une query avec wordpress pour récupérer les
            //post de type drink uniquement
            $drinks = new WP_Query( ["post_type" => "drink"] );
            //On fait une deuxième boucle wordpress mais qui cette
            //fois ci, bouclera sur la query qu'on a fait juste au
            //dessus. (la boucle par défaut récupérant les infos de
            //la page/l'article sur lequel on se trouve actuellement)
            while($drinks->have_posts()): $drinks->the_post();
                //ainsi, le the_title() de cette boucle ne renverra pas
                //la même valeur que le the_title() de la boucle par défaut.
                echo '<a href="';
                //the_permalink() permet de  récupérer le lien vers l'article/la page en question
                the_permalink();
                echo '">';                
				$image = get_field("illustration");

                echo '<img src="' . $image['url'] . '" alt="' . $image['alt'] . '" />';
                the_title();
                echo '</a>';

            endwhile;

            echo '</section>';
            ?>
		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php get_footer();
