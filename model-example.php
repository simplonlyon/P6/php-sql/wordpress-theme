<?php 
/*
Template Name: Exemple de Modèle
*/

?>
<?php

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
            <h1>Mon modèle de page</h1>
            <?php
            while ( have_posts() ) : the_post();
            //Ne marche qu'avec ACF si on a défini un champ test
            //et un champ affichage_p pour ce modèle de page.
                $test = get_field("test");
                echo $test;
                
                the_title();
                the_content();

                $champs = get_field("affichage_p");

                if(isset($champs[0])) {
                ?>  

                <p>la case est cochée</p>

                <?php
                }
            endwhile
            ?>
		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php get_footer();
