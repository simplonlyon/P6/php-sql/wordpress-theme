<?php

use Timber\Timber;

$context = Timber::get_context();
$context['post'] = Timber::get_post();

// $context['drinks'] = Timber::query_posts(['post_type' => 'drink']);

Timber::render("single-drink.twig", $context);
