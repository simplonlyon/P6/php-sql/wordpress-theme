<!-- On peut modifier un template-part directement plutôt que modifier
la page php à la racine, ça permet de faire des modifications un
peu plus localisées (et donc potentiellement moins de travail)
Pour ce faire, il faut bien reproduire la structure de dossier telle
qu'elle est dans le thème parent. -->

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	

	<h1>
		<!-- les fonctions the_title() et the_content() permettent
		d'afficher les champs titres et contenus de l'interface
		d'administration wordpress.
		Elles ne sont disponibles sous cette forme que lorsqu'on
		se trouve dans la boucle wordpress -->
		<?php the_title() ?>
	</h1>

	<p>
		<?php the_content() ?>
	</p>
</article><!-- #post-## -->
