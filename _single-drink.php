<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();
				echo "<h1>";
				the_title();
				echo "</h1>";

				echo "<h2>Ingrédients</h2><p>" . get_field("ingredients") . "</p>";
	
				if(get_field("alcoolise")) {
					echo "<p> Alcool : " . get_field("degre_dalcool") ."°</p>";
				}

				$image = get_field("illustration");

				echo '<img src="' . $image['url'] . '" alt="' . $image['alt'] . '" />';


			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
	<?php get_sidebar(); ?>
</div><!-- .wrap -->

<?php get_footer();
