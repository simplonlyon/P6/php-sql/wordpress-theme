<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/page/content', 'page' );

				/**
				 * On rajoute la zone de widget qu'on a enregistrée
				 * dans le functions.php à l'endroit où on veut qu'elle
				 * s'affiche (on peut tout à fait le mettre dans plusieurs
				 * fichiers php si on veut qu'elle soit à la fois dans les
				 * articles et dans les pages etc.)
				 */
				if( is_active_sidebar("widget_page") ) {
					dynamic_sidebar("widget_page");
				}

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php get_footer();
