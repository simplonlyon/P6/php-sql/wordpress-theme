<?php

use Timber\Timber;


class DrinkList extends WP_Widget {

    public function __construct() {
        /**
         * Premier argument : l'id unique du widget
         * Deuxième argument : le nom du wiget affiché dans l'admin
         * Troisième argument : les options du widgets
         * classname : le nom de la classe css qui sera mise dans le
         * before_widget pour ce widget
         * description : la description du widget qui sera affichée
         * dans l'admin
         */
        parent::__construct("drink_list", "Liste Boissons", [
            "classname" => "drink_list",
            "description" => "Un widget permettant de lister des boissons"
        ]);
    }
    /**
     * Méthode d'affichage du widget
     * @param array $args contient des valeurs données par la 
     * zone de widget
     * @param array $instance contient des valeurs données par la
     * zone d'administration du widget actuel
     */
    public function widget($args, $instance)
    {
        /**
         * On est clairement pas obligé de faire le widget via
         * timber, au contraire généralement ils sont plutôt fait
         * avec des echo et du html directement dans le widget
         * exemple : https://github.com/simplon-lyon/promo5-wordpress/blob/master/wp-content/themes/twentyseventeen-child/widgets/liste-projet.php
         */
        $context = Timber::get_context();
        $context["drinks"] = Timber::query_posts(["post_type" => "drink", "posts_per_page" => $instance["limit"]]);
        $context["args"] = $args;
        $context["instance"] = $instance;
        Timber::render("widgets/drink-list.twig", $context);
        
    }
    /**
     * Méthode qui affiche le formulaire d'administration du widget
     */
    public function form($instance)
    {
        $context = Timber::get_context();
        $context["instance"] = $instance;
        $context["fields"] = [
            "limit" => $this->createField("limit"),
            "image" => $this->createField("image")
        ];
        Timber::render("widgets/drink-list-admin.twig", $context);
    }

    private function createField($field) {
        return [
            "id" => $this->get_field_id($field),
            "name" => $this->get_field_name($field)
        ];
    }
    /**
     * Méthode de validation du formulaire d'administration.
     * Elle doit return un tableau associatif représentant
     */
    public function update($new_instance, $old_instance)
    {
        $instance = [];
        if(!empty($new_instance['limit']) && $new_instance['limit'] > -1 ) {
            $instance['limit'] = $new_instance['limit'];
        } else {
            $instance['limit'] = -1;
        }
        $instance["image"] = $new_instance["image"];
        return $instance;
    }



}