<?php

add_action("wp_enqueue_scripts", "style_loading");

function style_loading() {
    /**
     * wp_enqueue_style permet de mettre une feuille de style en "file d'attente"
     * le premier argument est l'identifiant de la feuille de style (on met ce qu'on veut)
     * le second est le chemin vers la feuille de style en question.
     * On récupère le chemin en utilisant la fonction get_template_directory_uri, qui, comme son nom l'indique nous
     * donne le chemin vers le dossier du thème parent. On lui concatène le style à mettre en file d'attente (style.css)
     */
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css' );
}

Timber::$dirname = array('templates', 'views');

/**
 * Un peu comme avec le style loading, ici on fait une function 
 * qui servira à charger les widgets qui sera ensuite appelée par
 * un add_acction spécifique
 */
function widget_loading() {
    //On include le fichier qui contient le widget
    include "widgets/drink-list.php";
    //on le register en donnant le nom de la classe
    register_widget("DrinkList");
}
//le fameux add_action qui chargera effectivement les widgets
add_action("widgets_init", "widget_loading");


function page_widget_init() {
    /**
     * Fonction permettant d'enregistrer une nouvelle zone de widget
     * dans l'administration.
     * name : le nom du widget affiché dans l'admin
     * id : l'id unique du widget à utiliser dans le code etc.
     * before_widget : la balise ouvrante qui viendra se metttre avant
     * chaque widget placé dans cette zone
     * after_widget : la balise fermante
     * before_title : la balise ouvrante qui viendra se mettre avant le
     * title des widgets
     */
	register_sidebar( array(
		'name'          => 'Widget de la Page',
		'id'            => 'widget_page',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'page_widget_init' );
